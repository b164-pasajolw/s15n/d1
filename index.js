console.log("Hello World!")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
// + - * / %

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

//Subtraction/Multiplication/division assignment operator (-=, *=, /=)

assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

//Multiple Operators and Parenthesis
/*
-when multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
- The operations were done in the following order:
1. 3 * 4 = 12
2. 12 / 5 = 2.4
3. 1 + 2 = 3
4. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

//Increment and Decrement Operator
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation.
//The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment".
++z;
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

//post-fix incrementation
//The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
z++;
console.log(z); //3 - The value of z was added with 1
console.log(z++); // the previous value of the variable is returned.
console.log(z); //4 a new value is now returned.

//pre-fix vs. post-fix incrementation
console.log(z++); //4
//console.log(z++); //5
//console.log(z++); //6
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately

//pre-fix and post-fix decrementation
console.log(--z); //5 - with pre-fix decremetation the result of subtraction by 1 is returned immediately

console.log (z--); //5 - with post-fix decrementation the result of subtraction by q is not immediately returned, instead the previous value is returned first
console.log(z); //4

//Type Coercion
//is the automatic or implicit conversion of values from one data type to another
let numA = '10' ;
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);

//Adding/Concatenating a string and a number will result to a string

let numC = 16; 
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

//the boolean "true" is also associated with the value of 1
let numE = true + 1; 
console.log(numE); //
console.log(typeof numE); //number

//the boolean "false" is also associated with the value of 0

let numF = false + 1;
console.log(numF); //number


//Comparison Operators
let juan = 'juan';

//(==) Equality Operator (checks whether the operands are equal/have the same content)
//Attempts to CONVERT AND COMPARE operands of different data types
//Returns a boolean value

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 =="1"); //true
console.log(0 == false) //true
console.log('juan' == 'JUAN'); //false, case sensitive;
console.log('juan' == juan); //true

//(!=) Inequality Operator
//Checks whether the operands are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1') //false
console.log(0 != false); //false
console.log('juan' != 'JUAN'); //true, case sensitive
console.log('juan' != juan); //false

// (===) Strictly Equality Operator
console.log('Strictly Equality Operator')
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false //type was checked, not the same data type
console.log(0 === false) //false //type was checked, not the same data type
console.log('juan' === 'JUAN'); //false, case sensitive;
console.log('juan' === juan); //true

// (!==) Strictly Inequality Operator
console.log('Strictly Inequality Operator')
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1') //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN'); //true, case sensitive
console.log('juan' !== juan); //false

//Relational Comparison Operators
//Check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("Greater Than")
//Greater than (>)
console.log(x > y); //false
console.log(w > y); //true

//Less than   (<)
console.log("Less Than")
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion - to change string to number
console.log(numString < 6000); //true - forced coercion - to change string to number
console.log(numString < "Jose"); //true - '5500' < "Jose"

//Greater Than or Equal to
console.log(w >= 8000); //true

//Less Than or Equal to
console.log(x <= y) //true
console.log(y <= y) //true

// LOGICAL OPERATORS 

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical AND Operator");
// Logical AND Operator (&& - Double Ampersand)
// Returns true if ALL operands are TRUE
let authorization1 = isAdmin && isRegistered; 
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered; 
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;


let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); //true

let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
// .length is a property of strings which determine the number of characters in the string.
console.log(registration1); //false - 

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true



console.log("Logical OR Operator");
// OR Operator ( || - Double Pipe)
// Returns TRUE if atleast one of the operands are TRUE


let userlevel = 100;
let userlevel2 = 65;

let guildRequirement1 = isRegistered || userlevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); //true

let guildRequirement2 = isAdmin || userlevel2 >= requiredLevel;
console.log(guildRequirement2); //false

console.log("Logical NOT Operator");
// NOT operator (!)
// turns a boolean into the opposite value 

let guildAdmin = !isAdmin || userlevel2 >= requiredLevel;
console.log(guildAdmin); //true

console.log(!isRegistered); //false
console.log(!isLegalAge); //false

let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite); //true - isAdmin original value is false

console.log(opposite2); //false - isLegalAge original value is true


// if, else if and else statement

// IF Statement
// if statement will run a code block if the condition specified is true or results to true

/*if(true){
	alert("We just run an if condition");
} */

let numG = -1;
if(numG < 0) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Welcome to Game Online!");
}

if(userLevel3 >= requiredLevel) {
	console.log("You are qualified to join the guild!")
} 

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("Thank you for joining the Admin!")
} else {
	console.log("You are not ready to be an Admin")
}

//ELSE Statement
//The "else" statement executes a block of codes if all other conditions are false

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noobies Guild!");
} else {
	console.log("You are too strong to be a noob. :(")
}

//else if statement
//else if executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if (userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the noobies guild.")
} else if(userlevel > 25){
	console.log("You're too strong to be a noob.")
} else if (userAge3 < requiredAge){
	console.log("You're too young to join the guild.")
} else {
	console.log("better luck next time.")
}

//if, else if and else statement with functions

function addNum(num1, num2) {
	//check if the numbers being passed are number types.
	//typeof keyword returns a string which tells the type of data that follows it
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types")
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers");
	}
}

addNum(5,'2')

//create log in function

function login(username, password) {
	//check if the argument paased are strings
	if(typeof username === "string" && typeof password === "string"){
		console.log("Both Arguments are strings.")

	
		/*
			nested if-else
				will run if the parent if statement is able to agree to accomplish its condition

			Mini-Activity:

			add another condition to our nested statement:
				-check if username is atleast 8 characters long
				-check if the password is atleast 8 characters long. 
				- show an alert which says "Thank you for logging in" add an else statement which will run if both condition were not met:
				-show an alert which says "Credentials too short."

	
			Strectch Goals:

				add an else if statement that if username is less than 8 characters
					-show an alert "Username is too short"
                    -add an else if statement if the password is less than 8 characters
                    -show an alert "password too short"

		*/



	} if (username.length >= 8 && password.length >= 8){
		console.log("Thank you for logging in")
	} else if(username.length < 8 ) {
		console.log("Username is too short")
	} else if (password.length < 8) {
		console.log("password too short")
	} else {
		console.log("Credentials too short")
	}

	/*else {
		console.log("One of the arguments is not a string")
		}*/
	}

login("jane", 123)

// function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.';
	}
	else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.'
	}
	else {
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);
//console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

if (message == 'Tropical storm detected.') {
	console.warn(message);
}

// TRUTHY and FALSY
//truthy
if(true) {
	console.log('Truthy');
}

if(1) {
	console.log('True')
}


if([]){
	console.log('Truthy')
}


//falsy
if(false){
	console.log('Falsy')
}

if(0){
	console.log('False')
}

if(undefined) {
	console.log('Falsy')
}

//Ternary Operator

/*
	Syntax: (updated else syntax of newest version)
 		(expression/condition) ? ifTrue : ifFalse;
Three operatnd of ternary operator:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is false

Ternary operators were created so that we can have an if-else statement in one line
*/

//Single Statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(`Results of ternary operator ${ternaryResult}`);

if(1<18) {
	console.log(true)
} else {
	console.log(false)
}

let price = 50000;

price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000")

let villain = "Harvey Dent";

villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villaineuos yet.")

//Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable

let a = 7;
a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));

//Multiple statement execution
let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = "Jane";
	return 'You are under the age limit';
}
 //parseInt for number data
let age = parseInt(prompt("What is your age?"));
console.log(age);


// ? specifies the "if"
// () specifies the condition
// : specifies the "else"
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`);


/*
	Mini-Activity s15

                        Create a function which will determine the color of shirt to wear depending on the day the user inputs.

                                Monday - Black
                                Tuesday - Green
                                Wednesday - Yellow
                                Thursday - Red
                                Friday - Violet
                                Saturday - Blue
                                Sunday - White

                        Log a message on an alert window:
                                "Today is <DayToday>, Wear <ColorOfTheDay>"
                        If the day input is out of range, log a message on an alert window:
                                "Invalid Input. Enter a valid day of the week."

                        Stretch Goal:
                        Check if the argument passed is a string:
                                -Log an alert if it is not:
                                        "Invalid Input. Please input a string."
                        Research and use .toLowerCase(), so a user can input in lowercase.

                        If you're done, send a screenshot of your browser showing your console and your use of the function in the hangouts.
*/


/*
function dayColor(day) {
	//check if the argument passed is a string
	if (typeof day === "string"){	
	
		if(day.toLowerCase() === "monday"){
			alert(`Today is ${day}. Wear Black`)
		}
		else if(day.toLowerCase() === "tuesday"){
			alert(`Today is ${day}. Wear Green`)
		}
		else if(day.toLowerCase() === "wednesday"){
			alert(`Today is ${day}. Wear Yellow`)
		}
		else if(day.toLowerCase() === "thursday"){
			alert(`Today is ${day}. Wear Red`)
		}
		else if(day.toLowerCase() === "friday"){
			alert(`Today is ${day}. Wear Violet`)
		}
		else if(day.toLowerCase() === "saturday"){
			alert(`Today is ${day}. Wear Blue`)
		}
		else if(day.toLowerCase() === "sunday"){
			alert(`Today is ${day}. Wear White`)
		}
		
	} else {
		alert("Invalid Input, Enter a valid day of the week.") 
	}
}

let day = prompt("Enter a day");

dayColor(day);

*/

//parseInt() - convert a string to be a number

//Switch Statement
/*
Syntax:
	Switch (expression/condition) {
	case value:
		statement;
		break;
	default:
		statement;
		break;
	}
*/

let hero = prompt("Type a hero").toLowerCase();
//.toLowerCase() - changes the string to lowercase

switch (hero) {
	case "jose rizal":
		console.log("National Hero of the Philippines");
		break;
	case "george washington":
		console.log("Hero of the American Revolution");
	case "hercules":
		console.log("Legendary Hero of the Greek");
		break;
	default:
		console.log("Please type again");
		break;
}

function roleChecker(role) {
	switch(role) {
		case "admin":
			console.log("Welcome Admin, to the dashboard.");
		break;
		case "user":
			console.log("You are not authorized to view this page.");
		case "guest":
			console.log("Go to the registration page to register");
		default:
			console.log("Invalid Role")
			//by default, your switch ends with default case, so therefore, even if there is no break keyword in your default case, it will not run anything else.
	}
}

roleChecker("admin");


//Try-Catch-Finally Statement
//this is used for error handling

function showIntensiveAlert(windSpeed) {
	//Attempt to execute a code
	try {
		alerta(determineTyphoonIntensity(68));
	}catch(error) {
		//error/err are commonly used variable by developers for strong errors
		console.log(typeof error)

		//Catch errors within "try" statement
		console.warn(error.message)
	}
	finally {
		//Continue execution of code REGARDLESS of success ir failure of code execution in the 'try' block to handle/resolve errors.
		//optional
		alert('Intensity updates will show new alert')
	}

}

showIntensiveAlert(68);


//throw - user-defined exception
//Execution of the current function will stop

const number = 40;

try {
	if(number > 50) {
		console.log('Success');
	} else {
		//user-defined throw statement
		throw Error('The number is low');
	}

	//if throw executes, the below code does not execute
	console.log('hello')
}
catch(error) {
	console.log('An error caught');
	console.warn(error.message)
}
finally {
	console.log("Please add a higher number")
}

//another example

function getArea(width, height) {
	if(isNaN(width) || isNaN(height)) {
		throw "Parameter is not a number!"
	}
}

try {
	getArea(3, 'A');
}
catch(e) {
	console.error(e)
}
finally {
	alert("Number only")
}